      var socketio = io.connect();
      socketio.on("message_to_client",function(data) {
         //Append an HR thematic break and the escaped HTML of the new message
         document.getElementById("chatlog").appendChild(document.createTextNode(data['message']));
		 document.getElementById("chatlog").appendChild(document.createElement("hr"));
		 updateScroll("chatlog");
      });
 	socketio.on("newUser",function(data) {
         console.log(data);
         //Append an HR thematic break and the escaped HTML of the new message
         document.getElementById("chatlog").appendChild(document.createTextNode("New User " +"Connected to room"));
		 document.getElementById("chatlog").appendChild(document.createElement("hr"));
		 updateScroll("chatlog");
      });
 
      function sendMessage(){
         var msg = document.getElementById("message_input").value;
         socketio.emit("message_to_server", {message:msg});
		 document.getElementById("message_input").value = "";
      }
	  //updates the scroll to the bottom only if user hasn't scrolled up
	  function updateScroll(el){
		 var element = document.getElementById(el);
		 var currHeight = element.scrollHeight - element.clientHeight;
		 //allowing for margin of error.
		 var atBottom =  currHeight <= element.scrollTop+50;
         console.log("updating "+ currHeight + "; " + element.scrollTop + " " + atBottom);
		 if (atBottom) {
            var pane = document.getElementById("chatlog");
			pane.scrollTop = pane.scrollHeight;
         }
	  }
      $(document).ready(function() {
      $('#message_input').keydown(function(event) {
        if (event.keyCode == 13) {
            sendMessage();
            return false;
         }
    });
});